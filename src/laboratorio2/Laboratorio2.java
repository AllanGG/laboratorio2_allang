package laboratorio2;
import java.lang.reflect.Array;
import java.util.Objects;
import java.util.Scanner;

public class Laboratorio2 {


    public static void main(String[] args) {
        
        Scanner entero = new Scanner (System.in);
        Scanner caracter = new Scanner (System.in);
        
        boolean ciclo = true;
        
        while (ciclo){
            System.out.println(" <<<<< Bienvenido>>>>>\n Seleccione una opción: ");
            System.out.println(" 1. Registro de persoans\n 2. Registro de mascotas\n 3. Adoptar mascota\n 4. Salir\n Opcion:");
            int opcion = entero.nextInt();
            switch (opcion){
                case 1:
                    personas(entero,caracter);
                    break;
                case 2:
                    mascotas(entero,caracter);
                    break;
                case 3:
                    adoptar(entero,caracter);
                    break;
                case 4:
                    ciclo = false;
                    break;
            
            }
        }
        
    }
    
    public static void personas(Scanner entero,Scanner caracter){
        System.out.println("Ingrese el numero de cedula:");
        Integer cedula = entero.nextInt();
        System.out.println("Ingrese el nombre y apellido:");
        String nomPersonas = caracter.nextLine();
        System.out.println("Ingrese el genero:");
        String genero = caracter.nextLine();
        
        Personas.ListaPersonas.add(new Personas(cedula,nomPersonas,genero));
        
        for(int conta=0;conta <=Personas.ListaPersonas.size()-1;conta++){
            Personas dato = Personas.ListaPersonas.get(conta);
            Integer c= dato.getCedula();
            String n= dato.getNomPersona();
            String g= dato.getGenero();
            System.out.println(c+n+g); 
        }
    }
      
    public static void mascotas(Scanner entero,Scanner caracter){
        System.out.println("Ingrese el ID de la mascota");
        Integer idMascota = entero.nextInt();
        System.out.println("Ingrese el nombre de la mascota:");
        String nomMascota = caracter.nextLine();
        System.out.println("Ingrese el tipo de mascota (Perro/Gato):");
        String tipo = caracter.nextLine();
        System.out.println("Ingrese el estado de la mascota(Disponible/Adoptado)");
        String estado = caracter.nextLine();
        
        Mascotas.ListaMascotas.add(new Mascotas(idMascota,nomMascota,tipo,estado));
        
        for(int conta=0;conta <= Mascotas.ListaMascotas.size()-1;conta++){
            Mascotas dato = Mascotas.ListaMascotas.get(conta);
            Integer c= dato.getIdMascota();
            String n= dato.getNomMascota();
            String t= dato.getTipo();
            String e= dato.getEstado();
            System.out.println(c+n+t+e);   
        }  
    }
    
    public static void adoptar(Scanner entero,Scanner caracter){
        
        System.out.println("Ingrese el numero de cedula del adopatante:");
        Integer adoptante = entero.nextInt();
        boolean  encontrado = false;
        
        for(int conta=0;conta <=Personas.ListaPersonas.size()-1;conta++){
            Personas dato = Personas.ListaPersonas.get(conta);
            if(Objects.equals(adoptante, dato.getCedula())){
                System.out.println("Animales disponibles para adoptar:\nID      Nombre      Tipo      Estado");
                for(int conta2=0;conta2 <= Mascotas.ListaMascotas.size()-1;conta2++){
                    Mascotas dato2 = Mascotas.ListaMascotas.get(conta2);
                    if(Objects.equals("Disponible", dato2.getEstado())){
                        Integer i= dato2.getIdMascota();
                        String n= dato2.getNomMascota();
                        String t= dato2.getTipo();
                        String e=dato2.getEstado();
                        System.out.println(i+"    "+n+"   "+t+"    "+e);
                    }
                }
                System.out.println("Ingrese el numero de ID del animal que deseas adoptar:");
                Integer adoptado = entero.nextInt();
                
                boolean encontrado2= false;
                for(int conta3=0;conta3 <=Mascotas.ListaMascotas.size()-1;conta3++){
                    Mascotas dato3 = Mascotas.ListaMascotas.get(conta3);
                    if((Objects.equals(adoptado, dato3.getIdMascota())) && (Objects.equals("Disponible", dato3.getEstado()))){
                        dato3.setEstado("Adoptado");
                        System.out.println("Animal adoptado con exito :)");
                        encontrado2=true; 
                    }
                }
                if(encontrado2==false){
                    System.out.println("Animal no registrado");
                }
                encontrado=true;
            }
        }
        if(encontrado == false){
            System.out.println("Debes registrarte para continuar");
        }    
    }   
}
